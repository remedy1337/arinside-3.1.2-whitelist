//Copyright (C) 2012 Laurent Matheo | lmame
//
//This file is part of ARInside.
//
//    ARInside is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, version 2 of the License.
//
//    ARInside is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

/*
--LMA: This class is a "white list", works with new parameters in settings.ini
whitelist_file=whitelist.ini
whitelist_only_load_what_is_defined=1

--We put in the whitelist the objects we want to analyze, and only those ones.
*/

#include "stdafx.h"
#include "WhiteList.h"
#include "../ARInside.h"

// with this its easier to change container type later, if needed
typedef vector<string> WhitelistContainer;
typedef vector<string>::iterator WhitelistIterator;

CWhiteList::CWhiteList(void)
{
	whitelist_loaded=false;
}

CWhiteList::~CWhiteList(void)
{
}

//LMA: Trim
/* static */
void CWhiteList::trim( string& s )
{
	// Remove leading and trailing whitespace
	static const char whitespace[] = " \n\t\v\r\f";
	s.erase( 0, s.find_first_not_of(whitespace) );
	s.erase( s.find_last_not_of(whitespace) + 1U );
}

/*
--LMA: 2012/10/29
--Loading the white list.
--1 object per line
form=HPD:Help Desk
acl=
filter=
escalation=
container=
charm=
img=
*/
bool CWhiteList::LoadFromServer(string filename)
{
	//LMA: 2do: Loading what is allowed :p
	//It's a very crude file load code :p don't bother.
	//taken from ConfigFile.cpp, in the best of worlds, class :p

	if (filename==""){
		cout << "No Whitelist" << endl;
		return true;
	}

	cout << "Trying to load White list " << filename.c_str() << endl;

	std::ifstream is( filename.c_str() );	
	if( !is ) {
		cerr << "WhiteList not found:  " << filename.c_str();
		return false;
	}
	
	//Reading the file.
	typedef string::size_type pos;
	const string& delim  = "=";  // separator
	const string& comm   = "#";    // comment
	const pos skip = delim.length();        // length of separator
	const string& sentry = "EndConfigFile";     // end of file sentry (should be useless :p)
	bool is_found=false;
	string nextline = "";  // might need to read ahead to see where value ends

	while( is || nextline.length() > 0 )
	{
		// Read an entire line at a time
		string line;
		if( nextline.length() > 0 )
		{
			line = nextline;  // we read ahead; use it now
			nextline = "";
		}
		else
		{
			std::getline( is, line );
		}

		// Ignore comments
		line = line.substr( 0, line.find(comm) );

		// Check for end of file sentry
		if( sentry != "" && line.find(sentry) != string::npos ){
			is.close();
			return true;
		}

		// Parse the line if it contains a delimiter
		pos delimPos = line.find( delim );
		if( delimPos < string::npos )
		{
			// Extract the key
			string key = line.substr( 0, delimPos );
			line.replace( 0, delimPos+skip, "" );

			// See if value continues on the next line
			// Stop at blank line, next line with a key, end of stream,
			// or end of file sentry
			bool terminate = false;
			while( !terminate && is )
			{
				std::getline( is, nextline );
				terminate = true;

				string nlcopy = nextline;
				CWhiteList::trim(nlcopy);
				if( nlcopy == "" ) continue;

				nextline = nextline.substr( 0, nextline.find(comm) );
				if( nextline.find(delim) != string::npos )
					continue;
				if( sentry != "" && nextline.find(sentry) != string::npos )
					continue;

				nlcopy = nextline;
				CWhiteList::trim(nlcopy);
				if( nlcopy != "" ) line += "\n";
				line += nextline;
				terminate = false;
			}

			// Cleaning...
			CWhiteList::trim(key);
			CWhiteList::trim(line);

			cout << "[WhiteList] Loading object type " << key << " value " << line << endl;

			//We don't care of multiple values :p it's a vector for Christ Sake...
			is_found=false;
			if (key=="form"){
				schemas.push_back(line);
				is_found=true;
			}

			if (key=="acl"){
				actlinks.push_back(line);
				is_found=true;
			}

			if (key=="filter"){
				filters.push_back(line);
				is_found=true;
			}

			if (key=="escalation"){
				escalations.push_back(line);
				is_found=true;
			}

			if (key=="container"){
				containers.push_back(line);
				is_found=true;
			}

			if (key=="charm"){
				menus.push_back(line);
				is_found=true;
			}

			if (key=="img"){
				images.push_back(line);
				is_found=true;
			}

			if(!is_found){
				cerr << "[WhiteList] Invalid key word " << key;
			}

		}
	}

	is.close();
	whitelist_loaded=true;
	cout << "Finished loading whitelist " << filename << endl;

	//Sorting time
	std::sort(schemas.begin(), schemas.end());
	std::sort(actlinks.begin(), actlinks.end());
	std::sort(filters.begin(), filters.end());
	std::sort(escalations.begin(), escalations.end());
	std::sort(containers.begin(), containers.end());
	std::sort(menus.begin(), menus.end());
#if AR_CURRENT_API_VERSION >= AR_API_VERSION_750
	std::sort(images.begin(), images.end());
#endif


	return true;
}

//LMA: Is WhiteList Loaded?
bool CWhiteList::CheckLoaded()
{
	return whitelist_loaded;
}

/*
--LMA: Object Count.
*/
size_t CWhiteList::GetCountOf(unsigned int nType)
{
	switch (nType)
	{
	case ARREF_SCHEMA: return schemas.size();
	case ARREF_ACTLINK: return actlinks.size();
	case ARREF_FILTER: return filters.size();
	case ARREF_ESCALATION: return escalations.size();
	case ARREF_CONTAINER: return containers.size();
	case ARREF_CHAR_MENU: return menus.size();
#if AR_CURRENT_API_VERSION >= AR_API_VERSION_750
	case ARREF_IMAGE: return images.size();
#endif
	case ARREF_ALL:
		{
			size_t totalCount = schemas.size();
			totalCount += actlinks.size();
			totalCount += filters.size();
			totalCount += escalations.size();
			totalCount += containers.size();
			totalCount += menus.size();
#if AR_CURRENT_API_VERSION >= AR_API_VERSION_750
			totalCount += images.size();
#endif
			return totalCount;
		}		
	default: return 0;
	}
}

//LMA: Is an object in the WhiteList?
bool CWhiteList::Contains(unsigned int nType, const char *objName)
{
	WhitelistContainer *list = NULL;

	switch (nType)
	{
	case ARREF_SCHEMA: list = &schemas; break;
	case ARREF_ACTLINK:	list = &actlinks; break;
	case ARREF_FILTER: list = &filters; break;
	case ARREF_ESCALATION: list = &escalations; break;
	case ARREF_CONTAINER: list = &containers; break;
	case ARREF_CHAR_MENU: list = &menus; break;
#if AR_CURRENT_API_VERSION >= AR_API_VERSION_750
	case ARREF_IMAGE: list = &images; break;
#endif
	default: return false;
	}
	
	if (list != NULL)
	{
		WhitelistIterator blEndIt = list->end();
		WhitelistIterator blFindIt = lower_bound(list->begin(), blEndIt, objName);
		
		if (blFindIt != blEndIt && (*blFindIt).compare(objName) == 0)
			return true;
	}
	return false;
}

//LMA: map???
//LMA: This function removes all items from the list and let only those 
//specified in the white list.
//The list is changed in place so the caller should backup
//numItems if necessary. 
//1/2/3/4 nb=4 => 1/3/3/4 nb=2
//2 is "removed", rather overwritten and 3 takes its place so nb=2 so "/3/4"
//will never be read
//Shouldn't be used anymore :(
void CWhiteList::Exclude(unsigned int nType, ARNameList *objNames)
{
	cout << "Before WhiteList for object type " << nType << ", old count is " << objNames->numItems << endl;

	//No WhiteList, so exit :)	
	if (!whitelist_loaded){
		cout << "No WhiteList defined, skipping Exclude " << endl;
		return;
	}

	if (objNames == NULL || objNames->numItems == 0) return;	

	WhitelistContainer *list = NULL;

	switch (nType)
	{
	case ARREF_SCHEMA: list = &schemas; break;
	case ARREF_ACTLINK:	list = &actlinks; break;
	case ARREF_FILTER: list = &filters; break;
	case ARREF_ESCALATION: list = &escalations; break;
	case ARREF_CONTAINER: list = &containers; break;
	case ARREF_CHAR_MENU: list = &menus; break;
#if AR_CURRENT_API_VERSION >= AR_API_VERSION_750
	case ARREF_IMAGE: list = &images; break;
#endif
	default: return;
	}

	//No need, nothing in it :p
	//We have to "empty" the list because nothing in the WhiteList
	if (list->size()==0){
		cout << "No whitelist defined for object type " << nType << ", skipping exclude and setting it to 0." << endl;
		objNames->numItems=0;
		return;
	}

	CARInside* pInside = CARInside::GetInstance();
	WhitelistIterator blEndIt = list->end();
	WhitelistIterator blBegIt = list->begin();
	unsigned int curObjWriteIndex = 0;
	unsigned int curObjReadIndex = 0;
#if AR_CURRENT_API_VERSION >= AR_API_VERSION_764
	size_t reservedLen = strlen(AR_RESERV_OVERLAY_STRING);
	bool checkForOverlayAndCustomName = (pInside->appConfig.bOverlaySupport && pInside->CompareServerVersion(7,6) >= 0);
#endif

	while (curObjWriteIndex < objNames->numItems)
	{
		string searchFor(objNames->nameList[curObjReadIndex]);

#if AR_CURRENT_API_VERSION >= AR_API_VERSION_764
		// In version 7.6.04 it's not allowed to create objects with "__o" and "__c"
		// at the end (see ARERR8858). Thats great, because now we don't need to
		// check, if this is really a custom- or overlay-object. Simply check if the
		// object name ends with that.
		if (checkForOverlayAndCustomName && (CUtil::StrEndsWith(searchFor, AR_RESERV_OVERLAY_STRING) || CUtil::StrEndsWith(searchFor, AR_RESERV_OVERLAY_CUSTOM_STRING)))
		{
			searchFor.resize(searchFor.length() - reservedLen);
		}
#endif

		// lets see if we could find the current object name in the WhiteList
		WhitelistIterator blFindIt = lower_bound(blBegIt, blEndIt, searchFor);		

		//Case where there is only one item...
		if (blFindIt == blEndIt || (*blFindIt).compare(searchFor) != 0)
		{
			// not found, remove it
			++curObjReadIndex;
			--objNames->numItems;
		}
		else
		{
			// this object should be kept in the list
			if (curObjWriteIndex != curObjReadIndex)
			{
				memcpy(objNames->nameList[curObjWriteIndex], objNames->nameList[curObjReadIndex], sizeof(ARNameType));
			}
			++curObjReadIndex;
			++curObjWriteIndex;
			cout << "[WhiteList] We found " << searchFor.c_str() << endl;
		}

	}


	cout << "After WhiteList for object type " << nType << ", New count is " << objNames->numItems << endl;
}


//LMA: 2do: check at the end to see if all whitelist objects have been found or not.
//Works only if "map" :p
