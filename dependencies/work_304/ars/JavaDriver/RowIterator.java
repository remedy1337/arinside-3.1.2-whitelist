/*****************************************************************************/
/*                                                                           */
/*                                                                           */
/*   �Copyright  2009 � 2012 BMC Software, Inc.                              */
/*   BMC, BMC Software, the BMC logos and other BMC marks are trademarks or  */
/*   registered trademarks of BMC Software, Inc. in the U.S. and /or         */
/*   certain other countries.                                                */
/*****************************************************************************/

package com.bmc.arsys.demo.javadriver;

import com.bmc.arsys.api.*;

class RowIterator implements IARRowIterator
{
   public RowIterator()
   {
   }

   public void iteratorCallback(Entry entry)
   {
      System.out.println("currentEntryId = " + entry.getEntryId());

      for (Value val : entry.values())
      {
         System.out.println(val.toString());
      }
   }

}