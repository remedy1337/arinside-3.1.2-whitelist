/*****************************************************************************/
/*                                                                           */
/*                                                                           */
/*   �Copyright  2009 � 2012 BMC Software, Inc.                              */
/*   BMC, BMC Software, the BMC logos and other BMC marks are trademarks or  */
/*   registered trademarks of BMC Software, Inc. in the U.S. and /or         */
/*   certain other countries.                                                */
/*****************************************************************************/

package com.bmc.arsys.demo.javadriver;

public class SyncObject
{
   boolean flag = false;

   public SyncObject( boolean bFlag )
   {
      flag = bFlag;
   }
   public synchronized boolean getFlag( )
   {
      return flag;  
   }

   public synchronized void setFlag( boolean bFlag )
   {
      flag = bFlag;  
   }

}