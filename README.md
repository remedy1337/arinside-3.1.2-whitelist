Hi :)

**Author (more like small contributor actually :p ):**

Laurent Matheo (https://communities.bmc.com/communities/people/lmame)
.

**Credits:**

This is a side work on arinside great tool (http://arinside.org/) from John Luthgers (jls17) (I hope other contributors forgive me for not mentionning them...).


**Why?**

This is something I'm working those last days since my company decided not to give me some work to do , mainly to check how arinside was coded, and I had a small idea.
Sometimes I just wanted to see and document one form or a couple of forms or objects (acl, filters...) and, well... arinside is always "full" and takes a while. Migrator takes a while (at least took it's been a while I didn't test it).
When I checked arinside I saw that there was a "blacklist" functionnality but it's not really user friendly  it needs to be a packing list and to list all objects you don't want to see... When you only want one object that could prove to be difficult 
I think there could be some problems anyway, if you remove all forms but one in the packing list, so one form remains and it'll crash because it needs the schema list for all other objects (assertion error).
At least that what happened when I played with it.


**So what?**

So I thought about doing a "whitelist". Basically I would list in a simple text file what would be exported actually, for example a couple of active links, and forms.
This way it should be much faster to render and in those cases you don't really care of the whole layout and relationships, just to get the details of the objects.
This was done especially for the overlays, to see the difference between the base and the overlay for example and  for small documentation

**Whitelist, how?**

In the "settings.ini" I added a "WhiteList" parameter asking for a filename (for example "whitelist.ini") that respects this format:



```
#!c++

<object type>=<object name>
```


//<Object type> //beeing:
form, acl, filter, charm, container, escalation, img
//<Object name>// is the object name, but case sensitive.

It also automatically discards whatever is commented by "#"

Example of "whitelist.ini":


```
#!c++

#Hello this is a comment 
form=HPD:Help Desk #this is also a comment.
form=CHG:Infrastructure Change

acl=xxxxxxx
acl=yyyyyy
filter=xxxxx
```



If "WhiteList" parameter isn't in settings.ini or is empty, the WhiteList functionnality will be off and you'll have your plain old vanilla arinside.
If you give a filename, only the objects listed in the whitelist will be fully detailed, perhaps with relationships, but those relationships won't be detailed (or will be if they are in the whitelist);
If you enter a form, views and fields will be fully detailed.
So if you do just this:


```
#!c++

form=HPD:Help Desk
```



That means that form "HPD:Help Desk" will be fully documented (form, views, fields, overlays), but you won't have any active links, filters and so on.
As you didn't enter any active links (for example), the active link list will be empty and no active link will be documented or even listed.


**How?**

I used arinside 3.0.4 source code and coded a "whitelist". I'll add more details when the code will be on svn.
I tried to adapt as much as possible and change nothing, adding a new "whitelist" class and a couple of code, not changing anything to existing code to preserve arinside.
Clearly the "whitelist" has never been thought when coding arinside and the logic behind the code made it slower than it could have been. For example you have to load the whole schema list, "just in case" because it's the basis.
You also have to get most of the details even if you don't use them.
But in the end it's really not that important, you would gain like... 20-30 seconds, you can live with it :)


**To do?**

For now it's a "cruel" whitelist, meaning that if no activelinks are listed in the whitelist, no activelink will be loaded, hence you wouldn't see the relationships.
I guess there could be a mode where you list forms and see relationships for the form, but I don't know the amount of work that would be needed, I'm pretty green on arinside...


**Where to get the source code?**

It's available in a public GIT:
https://bitbucket.org/remedy1337/arinside-3.1.2-whitelist/


**How to build it?**

I'm working on Windows right now, so you'll need MS Visual Studio 2008, so only its "solution" has been updated with the "whitelist" class:

-> lists/WhiteList.h,

-> lists/WhiteList.cpp,

Shouldn't be too hard to adapt / add those two files for other OS, I kept the same logic so should be linux / windows / whatever friendly 


**Binary and example:**

Get everything you need from the "/release/" folder here:
Everything is setup there.



**Performances?**

To output this (so only those are fully detailed):


```
#!c++

form=HPD:Help Desk
form=CHG:Infrastructure Change
acl=SHR:LHP:FormOpenCreateCopyRMS_InVF
acl=ASI:ASD:ReturnSelected_000
filter=PDL:EAU:Retrieve[46]SetCIDefault
filter=_CAT:INT:ANN_G_Integ_510_PushToUser_Modify`!
filter=CMDB:Instance:InvokeCMDBEngine01
escalation=SYS:NPC:TriggerGroupNotificationsPool3
escalation=_CAT:INT:Annuaire_DeleteData
container=AAS:ATV:CancelTasks
container=_CAT:INT:ANN_Trans
charm=_CAT:INT:PPT:PeopleTemplate
charm=AAS:SGP:SupportOrganization
img=application-tab-button-open-state

```


It took 73 seconds.